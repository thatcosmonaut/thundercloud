class ArtistsController < ApplicationController
  load_and_authorize_resource

  def index
    @artists = Artist.all
  end

  def show
    @artist = Artist.find(params[:id])
    @albums = @artist.albums
    @products = @artist.products
  end
end
