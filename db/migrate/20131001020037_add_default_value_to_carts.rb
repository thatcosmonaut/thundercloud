class AddDefaultValueToCarts < ActiveRecord::Migration
  def change
    change_column :carts, :order_total, :integer, default: 0
  end
end
