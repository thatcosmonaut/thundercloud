Thundercloud::Application.routes.draw do
  ActiveAdmin.routes(self)

  devise_for :users

  root to: "artists#index"

  match "/blog" => "static_pages#blog", via: :get
  match "/about" => "static_pages#about", via: :get

  resources :artists
  resources :products, only: [:index, :show]
  resources :line_items, only: [:create, :destroy]
  resources :carts, only: [:show] do
    member do
      get :checkout
      get :confirm
    end
  end

  resources :orders, only: [:show]
end
