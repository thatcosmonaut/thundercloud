require 'spec_helper'

describe LineItem do
  let (:cart) { FactoryGirl.create(:cart) }
  let (:product) { FactoryGirl.create(:product, name: "Album", description: "blah", price: 1500) }

  describe :construct_from_product do
    it 'creates a line item from a product with correct attributes' do
      line_item = LineItem.construct_from_product(cart, product)
      line_item.save
      expect(line_item.price).to eq(1500)
      expect(line_item.quantity).to eq(1)
      expect(line_item.product).to eq(product)
      expect(line_item.itemable).to eq(cart)

      line_item = LineItem.construct_from_product(cart, product)
      line_item.save
      expect(line_item.quantity).to eq(2)
    end
  end
end
