require 'spec_helper'

describe Cart do
  describe :update_total do

    it 'should update order total properly' do
      cart = Cart.new
      line_item = FactoryGirl.create(:line_item, price: 1500, quantity: 1)
      cart.line_items << line_item
      cart.update_total

      expect(cart.order_total).to be(1500)

      line_item_two = FactoryGirl.create(:line_item, price: 1000, quantity: 2)
      cart.line_items << line_item_two
      cart.update_total

      expect(cart.order_total).to be(3500)
    end
  end

  describe :generate_token do
    it 'should generate unique tokens for each cart' do
      tokens = []
      100.times do
        tokens << FactoryGirl.create(:cart).token
      end
      expect(tokens.uniq.length).to eq(100)
    end
  end
end
